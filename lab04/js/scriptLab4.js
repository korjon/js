


var animalImagesArr =["images/animals/cat.PNG", "images/animals/dog.PNG",
"images/animals/duck.PNG","images/animals/lion.PNG","images/animals/ostrich.PNG"
,"images/animals/panda.jpg","images/animals/polarbear.jpg",
"images/animals/zebra.PNG"];

var currentImageIndex = 0;


function preloadAllImages()
{
  document.getElementById('animalImg1').src = animalImagesArr[0];
  var list = document.getElementsByClassName('imageHolderDiv');
  var imgList = list[0].querySelectorAll('img');

  for(i = 0; i < imgList.length; i++)
  {
    imgList[i].src = animalImagesArr[i];
  }
}

function imageViewer()
{
  var list = document.getElementsByClassName('imageHolderDiv');
  var imgList = list[0].querySelectorAll('img');
  imgList[currentImageIndex].style.display = 'block';
}

function imageGallery()
{
  var skip = document.getElementById(this.id);
  var list = document.getElementsByClassName('imageHolderDiv');
  var imgList = list[0].querySelectorAll('img');

  if (skip.id == "btn-first")
  {
    imgList[currentImageIndex].style.display = 'none';
    currentImageIndex = 0;
    document.getElementById('imgNberSpan').innerHTML = currentImageIndex + 1;
    imageViewer();
  }


  if (skip.id == "btn-last")
  {
    imgList[currentImageIndex].style.display = 'none';
    currentImageIndex = 7;
    document.getElementById('imgNberSpan').innerHTML = currentImageIndex + 1;
    imageViewer();
  }




  if(skip.id == "btn-next")
  {
    if(currentImageIndex > 6)
    {
      imgList[currentImageIndex].style.display = 'none';
      currentImageIndex = 0;
      document.getElementById('imgNberSpan').innerHTML = currentImageIndex + 1;
      imageViewer();
    }
    else
     {
       imgList[currentImageIndex].style.display = 'none';
       currentImageIndex++;
       document.getElementById('imgNberSpan').innerHTML = currentImageIndex + 1;
       imageViewer();

     }
  }


  if (skip.id == "btn-previous")
  {
    if(currentImageIndex < 1)
    {
      imgList[currentImageIndex].style.display = 'none';
      currentImageIndex = 7;
      document.getElementById('imgNberSpan').innerHTML = currentImageIndex + 1;
      imageViewer();
    }
    else
     {
       imgList[currentImageIndex].style.display = 'none';
       currentImageIndex--;
       document.getElementById('imgNberSpan').innerHTML = currentImageIndex + 1;
       imageViewer();
     }
  }

}

function process()
{
  console.log('starts script');
  preloadAllImages();
  document.getElementById("btn-next").addEventListener("click", imageGallery);
  document.getElementById("btn-previous").addEventListener("click", imageGallery);
  document.getElementById("btn-first").addEventListener("click", imageGallery);
  document.getElementById("btn-last").addEventListener("click", imageGallery);
  console.log('ends script')
}

process();
